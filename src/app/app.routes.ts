import { Routes } from '@angular/router';
import { SplashComponent } from './splash/splash.component';
import { TicketsComponent } from './tickets/tickets.component';

const appRoutes: Routes = [
    {
        path: 'splash', component: SplashComponent
    },
    {
        path: '', redirectTo: '/splash', pathMatch: 'full'
    },
    {
        path: 'tickets', component: TicketsComponent
    }

];

export { appRoutes };