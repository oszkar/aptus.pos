const app = require('electron').app;
const BrowserWindow = require('electron').BrowserWindow;

app.on('ready', _ => {

    console.log('Starting Aptus.POS');
    console.log('Loading app from file://'+ __dirname +'/dist/index.html');

    var win = new BrowserWindow({ width: 1024, height: 800, frame:true, fullscreen:false });
    win.on('close', _ => {
        win = null;
    });

    win.loadURL('file://' + __dirname + '/dist/index.html');

});